package com.info.mapper;

import com.info.model.Room;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


import java.util.List;

@Mapper
public interface RoomMapper {

    List<Room> selectRoomWithExpiredContractToday();

    List<Room> selectRoomWithValidContractToday();

    List<Room> selectRoomHasContractorToday();

    List<Room> selectRoomHasNoContractorToday();

    void updateBatchRoom(@Param("list") List<Room> list);


}
