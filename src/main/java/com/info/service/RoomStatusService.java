package com.info.service;

import com.info.mapper.RoomMapper;
import com.info.model.Enum.RoomStatus;

import com.info.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class RoomStatusService {
    @Autowired
    private RoomMapper roomMapper;

    public void updateRoomStatusWithResident() {
        List<Room> noContractorList = roomMapper.selectRoomHasNoContractorToday();
        noContractorList.forEach(room -> room.setRoomStatus(RoomStatus.VACANCY));

        List<Room> hasContractorList = roomMapper.selectRoomHasContractorToday();
        hasContractorList.forEach(room -> room.setRoomStatus(RoomStatus.IN_USE));

        List<Room> updateList = new ArrayList<>(noContractorList);
        updateList.addAll(hasContractorList);
        if (updateList.size() > 0) {
            roomMapper.updateBatchRoom(updateList);
        }
    }

    public void updateRoomStatusWithContract() {
        List<Room> roomExpiredContractList = roomMapper.selectRoomWithExpiredContractToday();
        roomExpiredContractList.forEach(room -> room.setRoomStatus(RoomStatus.VACANCY));

        List<Room> roomNewValidContractList = roomMapper.selectRoomWithValidContractToday();
        roomNewValidContractList.forEach(room -> {
            if (room.isShare()) {
                room.setRoomStatus(RoomStatus.USE_A_PART);
            } else {
                room.setRoomStatus(RoomStatus.IN_USE);
            }
        });

        List<Room> updateList = new ArrayList<>(roomExpiredContractList);
        updateList.addAll(roomNewValidContractList);
        if (updateList.size() > 0) {
            roomMapper.updateBatchRoom(updateList);
        }
    }

}
